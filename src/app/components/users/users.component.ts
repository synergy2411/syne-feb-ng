import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { IUser } from 'src/app/model/user.interface';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  // encapsulation: ViewEncapsulation.ShadowDom
})
export class UsersComponent implements OnInit {

  users: IUser[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.users = this.dataService.getUserData()
  }

  onVotesChange(votes: string) {
    this.users[0].totVotes = Number(votes)
  }

  onMoreInfo(usr: IUser) {
    alert(`Mr. ${usr.lastName.toUpperCase()} is working with ${usr.company}`)
  }

}
