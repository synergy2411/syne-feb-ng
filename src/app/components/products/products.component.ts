import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IProduct } from 'src/app/model/product.interface';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  productCollection: IProduct[] = [];

  constructor(
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.productService.getAllProducts()
      .subscribe(products => this.productCollection = products)
  }

  onProductSelected(product: IProduct) {
    this.router.navigate([`/products/${product.id}/${product.name}`], {
      queryParams: { productPrice: product.price }
    })
  }
}
