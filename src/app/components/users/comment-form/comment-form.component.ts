import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IComment } from 'src/app/model/comment.interface';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {

  constructor() { }

  @Output() addCommentEvent = new EventEmitter<IComment>()

  ngOnInit(): void {
  }

  onAddItem(theForm: NgForm) {
    let newComment: IComment = { ...theForm.value };
    this.addCommentEvent.emit(newComment)
  }

}
