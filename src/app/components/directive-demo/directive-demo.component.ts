import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directive-demo',
  templateUrl: './directive-demo.component.html',
  styleUrls: ['./directive-demo.component.css']
})
export class DirectiveDemoComponent implements OnInit {


  tab = 1;

  toggleShow = false;

  todoCollection = [
    { label: "shopping", status: "pending" },
    { label: "planting", status: "completed" },
    { label: "insurance", status: "pending" },
    { label: "gorcery", status: "completed" },
  ]


  dynamicClasses = { 'my-border': this.toggleShow, 'my-feature': true };
  // dynamicClasses = ['my-border', 'my-feature'];

  dynamicStyles = {
    backgroundColor: '#333',
    color: '#fff'
  }

  constructor() { }

  revertTheme() {
    this.dynamicStyles.backgroundColor = "#333";
    this.dynamicStyles.color = "#fff";
  }
  changeTheme() {
    this.dynamicStyles.backgroundColor = "#fff";
    this.dynamicStyles.color = "#333";
  }

  onToggle() {
    this.tab = 3;
    this.dynamicClasses['my-border'] = !this.dynamicClasses['my-border']
    this.dynamicClasses['my-feature'] = !this.dynamicClasses['my-feature']
  }

  ngOnInit(): void {
  }

}
