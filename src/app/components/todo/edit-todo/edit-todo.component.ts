import { Input } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Component, OnInit, Output } from '@angular/core';
import { ITodo } from 'src/app/model/todo.model';

@Component({
  selector: 'app-edit-todo',
  templateUrl: './edit-todo.component.html',
  styleUrls: ['./edit-todo.component.css']
})
export class EditTodoComponent implements OnInit {

  @Input() todo: ITodo;
  @Output() saveTodoEvent = new EventEmitter()
  editLabel: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  onSaveTodo() { this.saveTodoEvent.emit({ todoId: this.todo.id, editLabel: this.editLabel }) }

}
