import { IUser } from "./user.interface";

export const USER_DATA: IUser[] = [{
  firstName: "bill",
  lastName: "gates",
  income: 50000,
  dob: new Date('Dec 21, 1964'),
  isWorking: true,
  totVotes: 120,
  company: "Microsoft",
  imagePath: "https://pbs.twimg.com/profile_images/1564398871996174336/M-hffw5a_400x400.jpg",
  comments: [
    { stars: 5, body: "like your work", author: "abc@test" },
    { stars: 4, body: "Love💕 your work", author: "foo@test" },
  ]
}, {
  firstName: "tim b.",
  lastName: "lee",
  income: 40000,
  dob: new Date('Jan 1, 1965'),
  isWorking: true,
  totVotes: 180,
  company: "World wide web",
  imagePath: "https://cdn.britannica.com/94/123894-050-53EC378E/Tim-Berners-Lee-2005.jpg",
  comments: [
    { stars: 3, body: "great work", author: "bar@test" },
  ]
}, {
  firstName: "steve",
  lastName: "jobs",
  income: 0,
  dob: new Date('Aug 23, 1971'),
  isWorking: false,
  totVotes: 210,
  company: "Apple",
  imagePath: "https://thumbs.dreamstime.com/b/steve-jobs-portrait-illustration-manually-painted-capturing-apple-genius-56617906.jpg",
  comments: []
}]
