import { HostBinding, HostListener } from '@angular/core';
import { Input } from '@angular/core';
import { ElementRef } from '@angular/core';
import { Directive } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  @Input() favColor: string;

  @HostBinding('style.backgroundColor') bgColor: string;
  @HostBinding('style.color') color: string;

  @HostListener('mouseenter')
  onMouseEnter() {
    this.bgColor = this.favColor;
    this.color = "#fff";
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.bgColor = 'transparent';
    this.color = '#000'
  }

  constructor() {
  }


  // constructor(private elementRef: ElementRef) {
  //   // console.log(this.elementRef);
  //   this.elementRef.nativeElement.style.backgroundColor = "#333"
  //   this.elementRef.nativeElement.style.color = "#fff"
  // }

}


// <div appHighlight></div>


// <div name="my-div"></div>
// [name="my-div"]{ color : red;}

// <div class="container"></div>
// .container
