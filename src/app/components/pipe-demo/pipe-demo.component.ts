import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-demo',
  templateUrl: './pipe-demo.component.html',
  styleUrls: ['./pipe-demo.component.css']
})
export class PipeDemoComponent implements OnInit {

  filteredStatus: string = '';

  isAscending = true;

  value = 199.20;
  pi = Math.PI;
  joiningDate = new Date("Dec 20, 2020");
  strValue = "Hello World";
  objOne = {
    name: "john Doe", age: 32, dob: new Date("Jan 20, 1985")
  }

  contactNumber = 987654321;

  todoCollection = [
    { label: "shopping", status: "pending" },
    { label: "insurance", status: "completed" },
    { label: "planting", status: "pending" },
    { label: "grocery", status: "completed" },
  ]

  inputLabel = '';

  promiseObj = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Here the data arrives")
    }, 2000)
  })




  onAddNewItem() {
    let newTodo = { label: this.inputLabel, status: 'pending' };
    this.todoCollection.push(newTodo)   // Impure Change
    // this.todoCollection = [newTodo, ... this.todoCollection];   // Pure Changes
    this.inputLabel = ''
  }

  constructor() { }

  ngOnInit(): void {
  }

}
