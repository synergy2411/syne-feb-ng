import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProduct } from 'src/app/model/product.interface';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  product: IProduct;
  productPrice: number;

  constructor(private route: ActivatedRoute, private productService: ProductService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => this.productPrice = params['productPrice'])

    // this.product = this.route.snapshot.data['products']
    this.route.data.subscribe(data => {
      this.product = data['products']
    })

    // this.route.params.subscribe(params => {
    //   const productId = params['productId'];
    //   this.productService.getProduct(productId)
    //     .subscribe(product => this.product = product)
    // })
  }

}
