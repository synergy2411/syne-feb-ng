import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ITodo } from '../model/todo.model';
import { Subject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class TodoService {

  // private todoCollection = new Subject<Array<ITodo>>();

  private baseURL = "https://syne-feb23-ng-default-rtdb.firebaseio.com/todos.json";

  constructor(private httpClient: HttpClient) { }

  getTodos() {
    return this.httpClient.get<Array<ITodo>>(this.baseURL)
    // return this.httpClient.get<Array<ITodo>>("http://localhost:3000/todos")
    // .subscribe((data : Array<ITodo>) => {
    //   this.todoCollection.next(data)
    // })
  }

  createTodo(label: string) {
    let newTodoItem: ITodo = { label };
    return this.httpClient.post("http://localhost:3000/todos", newTodoItem, {
      headers: {
        "Content-Type": "application/json"
      }
    })
  }

  deleteTodo(todoId: string) {
    return this.httpClient.delete(`http://localhost:3000/todos/${todoId}`)
  }

  updateTodo(todoId: string, label: string) {
    return this.httpClient.patch(`http://localhost:3000/todos/${todoId}`, { label }, {
      headers: {
        "Content-Type": "application/json"
      }
    })
  }
}
