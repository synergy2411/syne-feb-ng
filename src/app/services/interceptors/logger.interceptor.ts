import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpParams
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../auth.service';

@Injectable()
export class LoggerInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const clonedReq = request.clone({
      params: new HttpParams().set("auth", this.authService.getToken())
    });

    console.log("[LOGGER INTERCEPTOR]", clonedReq);
    return next.handle(clonedReq).pipe(tap(val => console.log("[TAP]", val)));
  }
}
