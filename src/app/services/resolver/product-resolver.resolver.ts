import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { IProduct } from 'src/app/model/product.interface';
import { ProductService } from '../product.service';

@Injectable({
  providedIn: 'root'
})
export class ProductResolverResolver implements Resolve<IProduct> {

  constructor(private productService: ProductService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<IProduct> {

    return this.productService.getProduct(route.params['productId']);
  }
}
