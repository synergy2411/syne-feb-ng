import { } from '@angular/core';

import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { tap, debounceTime, mergeAll, mergeMap, Subject, interval, Subscription, from, fromEvent, Observable } from 'rxjs';
import { take, map, filter } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-observable-demo',
  templateUrl: './observable-demo.component.html',
  styleUrls: ['./observable-demo.component.css']
})
export class ObservableDemoComponent implements AfterViewInit, OnInit {

  subject = new Subject();

  ngOnInit() {
    this.subject.subscribe(console.log)
  }

  onSubjectNext() {
    let count = 0;
    setInterval(() => {
      this.subject.next("PAckage : " + (count++))
    }, 1000)

  }

  onSubscribe() { }
  onUnsubscribe() { }


  ngAfterViewInit(): void { }



  // @ViewChild("search") inputSearchEl: ElementRef;

  // repos: Array<string>;

  // ngAfterViewInit(): void {

  //   let search$ = fromEvent(this.inputSearchEl.nativeElement, "input")

  //   search$.pipe(
  //     debounceTime(1000),
  //     map((event: any) => event.target.value),
  //     mergeMap((val) => {
  //       return ajax.getJSON(`https://api.github.com/users/${val}/repos`)
  //     })
  //   ).subscribe((data: Array<any>) => {
  //     // data.subscribe(response => console.log(response))
  //     this.repos = data.map(repo => repo.name)
  //   })


  // let interval$ = interval(1000)

  // interval$.pipe(
  //   take(8),
  //   map(val => val * 4),
  //   filter(val => val < 20)
  // ).subscribe(console.log)


  // let obs$ = new Observable((observer) => {
  //   setTimeout(() => observer.next("First Package"), 1000)
  //   setTimeout(() => observer.next("Second Package"), 3000)
  //   setTimeout(() => observer.next("Third Package"), 5000)
  //   // setTimeout(() => observer.error(new Error("Something went wrong")), 5500)
  //   setTimeout(() => observer.next("Fourth Package"), 6000)
  //   setTimeout(() => observer.complete(), 8000)

  // })

  // obs$.pipe(
  //   take(2),
  //   map(val => "The " + val),
  //   filter(val => val === "The First Package")
  // ).subscribe({
  //   next: data => console.log(data),
  //   error: err => console.error(err),
  //   complete: () => console.log("COMPLETED")
  // })

  // }



  // @ViewChild("theButton") btnEl: ElementRef;

  // friends = ["Joe", "Monica", "Ross", "Rachel"];
  // friends$ = from(this.friends);

  // interval$ = interval(1000)
  // unSub: Subscription;

  // ngAfterViewInit() {
  //   let fromEvent$ = fromEvent(this.btnEl.nativeElement, 'mouseenter');
  //   fromEvent$.subscribe((eventData) => console.log(eventData))
  // }



  // onSubscribe() {
  //   this.friends$.subscribe((data) => console.log(data))

  // this.unSub = this.interval$.subscribe({
  //   next: data => console.log(data),
  //   error: err => console.log(err),
  //   complete: () => console.log("[COMPLETED]")
  // })
  // }

  // onUnsubscribe() {
  //   this.unSub.unsubscribe();
  // }
}
