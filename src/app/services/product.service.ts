import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IProduct } from '../model/product.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseURL = "http://localhost:3000/products";

  constructor(private http: HttpClient) { }

  getAllProducts() {
    return this.http.get<IProduct[]>(this.baseURL)
  }

  getProduct(productId: string) {
    return this.http.get<IProduct>(`${this.baseURL}/${productId}`)
  }
}
