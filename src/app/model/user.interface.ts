import { IComment } from "./comment.interface";

export interface IUser {
  firstName: string;
  lastName: string;
  income: number;
  dob: Date;
  isWorking: boolean;
  totVotes: number;
  company: string;
  imagePath: string;
  comments: IComment[];
}
