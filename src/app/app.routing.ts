import { Routes } from "@angular/router";
import { AuthFormComponent } from "./components/auth-form/auth-form.component";
import { ObservableDemoComponent } from "./components/observable-demo/observable-demo.component";
import { PipeDemoComponent } from "./components/pipe-demo/pipe-demo.component";
import { ProductItemComponent } from "./components/products/product-item/product-item.component";
import { ProductsComponent } from "./components/products/products.component";
import { TodoComponent } from "./components/todo/todo.component";
import { UsersComponent } from "./components/users/users.component";
import { LoginGuard } from "./services/guard/login.guard";
import { ProductResolverResolver } from "./services/resolver/product-resolver.resolver";

export const APP_ROUTES: Routes = [{
  path: "",                          // http://localhost:4200
  redirectTo: "/auth",
  pathMatch: "full"
}, {
  path: "auth",                      // http://localhost:4200/auth
  component: AuthFormComponent
}, {
  path: "users",                     // http://localhost:4200/users
  component: UsersComponent
}, {
  path: "pipe-demo",                 // http://localhost:4200/pipe-demo
  component: PipeDemoComponent
}, {
  path: "observable-demo",            // http://localhost:4200/observable-demo
  component: ObservableDemoComponent
}, {
  path: "todos",
  component: TodoComponent,           // http://localhost:4200/todos
  canActivate: [LoginGuard]                    // Setup the gaurds at the gate
}, {
  path: "products",
  component: ProductsComponent,
  children: [
    {
      path: ":productId/:productName",
      component: ProductItemComponent,
      resolve: { products: ProductResolverResolver }
    }      // http://localhost:4200/products/xyz
  ]
}, {
  path: "lazy",
  loadChildren: () => import("./modules/lazy/lazy.module").then(m => m.LazyModule)
}, {
  path: "**",                         // http://localhost:4200/nowhere
  redirectTo: "/auth",
  pathMatch: 'full'
}
]
