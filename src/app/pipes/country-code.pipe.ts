import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countryCode'
})
export class CountryCodePipe implements PipeTransform {

  transform(value: number, code?: string) {
    switch (code) {
      case "USA": return "+01" + value;
      case "AUS": return "+11" + value;
      case "EUR": return "+21" + value;
      case "SIN": return "+36" + value;
      default: return "+91 " + value;
    }
  }

}


// create a pipe which reverse the string
