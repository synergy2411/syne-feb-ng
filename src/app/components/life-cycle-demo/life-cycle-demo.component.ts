import { ElementRef } from '@angular/core';
import { Input, ViewChild, ContentChild, AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, DoCheck, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-life-cycle-demo',
  templateUrl: './life-cycle-demo.component.html',
  styleUrls: ['./life-cycle-demo.component.css']
})
export class LifeCycleDemoComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

  @Input() title: string;

  @ViewChild('theParargraphVar') paragraphEl: ElementRef;
  @ContentChild("theHeadingVar") headingEl: ElementRef;

  // Dependency Injection
  constructor() {
    console.log("constructor", this.title);
    console.log("[CONSTRUCTOR]", this.paragraphEl);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("ngOnChanges", changes);
  }

  ngOnInit(): void {
    console.log("ngOnInit", this.title);
    console.log("[AFTER NG INIT]", this.paragraphEl);
    console.log("[AFTER NG INIT]", this.headingEl)
  }

  ngDoCheck(): void {
    console.log("ngDoCheck");
  }

  ngAfterContentInit(): void {
    console.log("ngAfterContentInit");
    console.log("[AFTER CONTENT INIT]", this.headingEl)
  }

  ngAfterContentChecked(): void {
    console.log("ngAfterContentChecked");
  }

  ngAfterViewInit(): void {
    console.log("ngAfterViewInit");
    console.log("[AFTER VIEW INIT]", this.paragraphEl);

  }

  ngAfterViewChecked(): void {
    console.log("ngAfterViewChecked");
  }

  ngOnDestroy(): void {
    console.log("ngOnDestroy");
  }



}
