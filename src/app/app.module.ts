import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { APP_ROUTES } from './app.routing';
import { AuthFormComponent } from './components/auth-form/auth-form.component';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';
import { DirectiveDemoComponent } from './components/directive-demo/directive-demo.component';
import { HeaderComponent } from './components/header/header.component';
import { LifeCycleDemoComponent } from './components/life-cycle-demo/life-cycle-demo.component';
import { ObservableDemoComponent } from './components/observable-demo/observable-demo.component';
import { PipeDemoComponent } from './components/pipe-demo/pipe-demo.component';
import { ProductItemComponent } from './components/products/product-item/product-item.component';
import { ProductsComponent } from './components/products/products.component';
import { EditTodoComponent } from './components/todo/edit-todo/edit-todo.component';
import { TodoComponent } from './components/todo/todo.component';
import { CommentFormComponent } from './components/users/comment-form/comment-form.component';
import { UserCommentsComponent } from './components/users/user-comments/user-comments.component';
import { UserImgComponent } from './components/users/user-img/user-img.component';
import { UserInfoComponent } from './components/users/user-info/user-info.component';
import { UsersComponent } from './components/users/users.component';
import { HighlightDirective } from './directives/highlight.directive';
import { EmployeeModule } from './modules/employee/employee.module';
import { CountryCodePipe } from './pipes/country-code.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { LoggerInterceptor } from './services/interceptors/logger.interceptor';

@NgModule({
  declarations: [           // Component / Directives / Pipes
    AppComponent, UsersComponent, UserImgComponent, UserInfoComponent,
    DialogBoxComponent, LifeCycleDemoComponent, DirectiveDemoComponent,
    UserCommentsComponent, HighlightDirective, PipeDemoComponent, CountryCodePipe, FilterPipe, SortPipe, CommentFormComponent, AuthFormComponent, ObservableDemoComponent, TodoComponent, EditTodoComponent, HeaderComponent, ProductsComponent, ProductItemComponent
  ],
  imports: [                // Modules - Built-in or Custom
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(APP_ROUTES),
    EmployeeModule,
    // LazyModule            // NEVER EVER DO THIS
  ],
  // providers: [DataService],            // Service
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoggerInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
