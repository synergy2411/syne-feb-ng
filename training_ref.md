# Breaks

- Tea Break : 11:00 (15 mins)
- Lunch : 01:00 (45 mins)
- Tea Break : 03:30 (15 mins)

# Angular Training Agenda

- Angular Building Blocks
  > Components
  > Directives
  > Pipes
  > Services
  > Modules
- Forms
  > Template driven
  > Model driven / Reactive
- RxJS Library
- Remote Server Call - HttpClient
- Router - Creating Single Page App
- App Deployment

# Angular

- Create SPA
- JavaScript Framework
- TypeScript supported
- Component based architecture
- MVC Pattern
- Mobile Apps (eg. ionic)
- Angular Universal (Server-side rendering - SSR)
- DOM Manipulation (Directives)
- 2 way data-binding
- XHR Calls
- Templates
- Routing
- Deal with forms
- Dependency Injection (Services)
- State Management
- UI Formatting (Pipes)
- Modular Approach (Modules)

# TypeScript

- superset of JavaScript
- supports OOPS concepts
- additional types
  > Primitive types : String, Number, Boolean, Symbol
  > Reference / Object type : Object, Array, Function, Date
  > Additional Types : any, void, null, unknown, undefined, enum, tuple, never
  > Custom Types : type keyword, classes, interface
- Type definition
- Better tooling
- Compile time type error

# Various JavaScript Libraries and Frameworks

- react : fast UI rendering; State Management (Redux/Context API), SPA (react-router-dom), Form Validation (formik, react-hhok-form, yup etc), XHR (axios / fetch API)
- vue ("Evan You"): Template driven approach, State Management (Vuex), form validation (Vuelidate), SPA (vue-router), XHR (axios / fetch API)
- jQuery : DOM Manipulation, AJAX, Animation
- knockout : MVVM Pattern, 2 way data-binding
- backbone : MVC at client-side
- stencil : Creates custom components
- polymer : Custom web rich component
- \*ember : very frequent changes

# Angular CLI Tool (ng)

- npm install @angular/cli@12 -g
- npm install @angular/cli@latest -g
- ng version
- ng new <workspace-name>
- cd frontend
- ng serve / npm start

# CLI Commands

- ng generate component path/to/component-name
- ng g c path/to/component-name
- ng g --help
- ng g c components/users/user-img
- ng g c components/users/user-info

# ViewEncapsulation

- Emulated (default) : both CSS will apply but local CSS will override the common global CSS rules
- None : no encapsulation; Local CSS will affect the other part of app
- ShadowDOM / Native : only local CSS will be applied. No effect of global CSS

- Comp A - None - h2 - color : 'blue'
- Comp B - ShadowDOM - h2

None vs Emulated : Emulated
None vs ShadowDOM : ShadowDOM

users - none : h2 {color : "red"}
app - shadowDOM : h2 {color : "blue}

# Component Types

- Smart / Container / Parent : UI business logic eg. users component
- Dump / Presentational / Child : user-image, user-info

- TypeScript
- Angular
- Bootstrap process
- Environment Setup
- Components : reusable piece of code
- Data binding
  : Property Binding
  : Event Binding
  : 2 way data binding - Banana in the box [(ngModel)]
- Nested Component Communication
  : PArent to child > @Input()+ Property Binding
  : Child to parent > @Output() + EventEmitter + Event Binding
- Content Projection / Data Projection
- Life Cycle Methods

- View Encapsulation
- Emulated : Local CSS rules will override common global CSS rules
- None : Local CSS will affect the other components
- ShadowDOM : Only Local CSS will apply

# Pure Pipe

- runs only for Pure changes

# Impure Pipe

- runs for both pure and impure; but not efficient

# Forms & Form Controls - CSS Classes & State

- ngValid / ngInvalid
- ngPristine / ngDirty
- ngTouched / ngUntouched

# Day 02 -

- Life Cycle Demo
  : @viewChild()
  : @ContentChild()
- Directive
  : Attribute - ngClass, ngStyle
  : Structural - *ngIf, *ngFor, \*ngSwitch
  : Custom Directive
  > @HostBinding()
  > @HostListener()
- Pipes
  : Built-in Pipes - currency, date, uppercase, lowercase, percent, async etc
  : Custom Pipes
  > Pure : runs for pure changes
  > Impure : runs on both pure and impure changes; NOT suitable for large dataset
- Form
  : Template Driven : HTML5 validation on the template; 'ngForm' Object
  : Model Driven / Reactive

- Form & Form Controls - Classes / States

# Observables :

- stream on which event occur at different time interval
- keeps an eye on data source
- can be consumed using observer object (next, error, complete)
- are both sync and async in nature
- are lazily executed (until subscribed)
- are cancelable
- power of Observable operators

# Promises :

- eagerly executed
- one shot
- NOT cancelable
- then().catch()

# Subject : are both Observable as well as Observer

- Observable : subscribe, pipe
- Observer : next, error, complete
- Types of Subject
  : Subject
  : BehaviourSubject : seed value
  : ReplaySubject : last n values (Cold Observable)
  : AsyncSubject : only supply the last emitted value

# JSON SERVER

> npm i json-server -g
> json-server --watch db.json

# Day 03

- Model driven form / Reactive Forms
  : FormControl
  : FormGroup
  : FormBuilder
  : FormArray
- Service
  : Singleton
  : Dependency Injection
  : Single Responsibility
- Hierarchical Injector
  : Root Module
  : Root Component
  : Other Component
- Observables
  : from, fromEvent, interval, observable etc
  : map, take, tap, merge, mergeAll, mergeMap, debounce, ajax etc
- HttpClient
  : json-server
  : GET, POST, DELETE, PATCH (CURD)

# Day 04

- Interceptor
- Routing
  : Create SPA
  : Nested routing
  : Route Parameter
  : Query Parameter
  : Programmatic navigation
  : Guards (canActivate, resolve, canLoad)
- Module
  : Eagerly Loaded Module
  : Lazily Loaded Module
- Debugging techniques
- App Deployment

- json-server --watch db.json

# Routing Terminologies

- Routes : defines which component to be loaded when certain path is matched
- RouterModule : to unlock routing config
- RouterOutlet <router-outlet> : provide the space on the template to load component
- RouterLink : creates the link and don't reload the page
- Router : allow to navigate programmatically
- CanActivate : setup the guard at the route level
- ActivatedRoute : access the current URL
- Resolver : pre-populate the data into the component

- Nested/Child Routing : nested routing component
- Route Parameter : dynamic value supplied to the URL (Part of URL)
- Query Parameter : key-value pair in URL after (?)/ additional information to URL

- ng g m modules/employee
- ng g c modules/employee/components/new-employee
