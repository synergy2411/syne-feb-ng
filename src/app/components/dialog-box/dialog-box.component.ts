import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.css']
})
export class DialogBoxComponent {

  @Output() closeEvent = new EventEmitter();

  onBtnClick() {
    this.closeEvent.emit();
  }

}
