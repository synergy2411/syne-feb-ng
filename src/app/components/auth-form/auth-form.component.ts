import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.css']
})
export class AuthFormComponent implements OnInit {

  username = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  password = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
    this.exclamationMarkValidator,
    // this.myValidator(["@", "!"])
  ]);
  hobbies = new FormArray<FormGroup<any>>([]);


  authForm: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.authForm = this.fb.group({
      username: this.username,
      password: this.password,
      hobbies: this.hobbies
    })
  }

  onAddNewHobby() {
    let newHobby = this.fb.group({
      name: new FormControl(),
      frequency: new FormControl()
    })
    this.hobbies.push(newHobby);
  }

  onRegister() {
    console.log(this.authForm);
    const { username, password } = this.authForm.value;
    this.authService.onRegister(username, password)
  }


  onUserLogin() {
    const { username, password } = this.authForm.value;
    this.authService.onLogin(username, password)

  }

  myValidator(arr: Array<string>) {
    return (control: AbstractControl) => {
      return arr.includes(control.value) ? null : { myvalidator: true }
    }
  }

  // Custom Validator
  exclamationMarkValidator(control: AbstractControl) {
    let hasExclamtion = control.value.indexOf("!") >= 0;
    return hasExclamtion ? null : { exclamation: true }
  }

  ngOnInit(): void {
  }

}
