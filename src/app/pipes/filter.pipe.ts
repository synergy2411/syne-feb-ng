import { Pipe, PipeTransform } from '@angular/core';

interface ITodo {
  label: string;
  status: string;
}

@Pipe({
  name: 'filter',
  pure: false         // default is true
})
export class FilterPipe implements PipeTransform {

  transform(todoColl: Array<ITodo>, filteredStatus: string) {
    console.log("[TRANSFORM]");
    if (filteredStatus === '') {
      return todoColl;
    }
    let resultArray = [];
    for (let todo of todoColl) {
      if (todo.status === filteredStatus) {
        resultArray.push(todo)
      }
    }
    return resultArray;
  }

}


// Create pipe that sort todoColl in ascending / decending order
