import { Pipe, PipeTransform } from '@angular/core';


interface ITodo {
  label: string;
  status: string;
}


@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(todoColl: ITodo[], isAscending: boolean, sortBy: string = 'label') {
    if (isAscending) {
      return todoColl.sort((a, b) => {
        if (a[sortBy] > b[sortBy]) {
          return 1
        } else if (a[sortBy] < b[sortBy]) {
          return -1
        } else {
          return 0
        }
      })
    } else {
      return todoColl.sort((a, b) => {
        if (a[sortBy] > b[sortBy]) {
          return -1
        } else if (a[sortBy] < b[sortBy]) {
          return 1
        } else {
          return 0
        }
      })
    }
  }

}
