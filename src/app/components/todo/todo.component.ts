import { Component, OnInit } from '@angular/core';
import { ITodo } from 'src/app/model/todo.model';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  todoCollection: ITodo[] = [];
  inputLabel: string = '';

  isEdit = false;

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    this.todoService.getTodos()
      .subscribe(data => this.todoCollection = data)
  }

  onAddItem() {
    this.todoService.createTodo(this.inputLabel)
      .subscribe((data: ITodo) => {
        this.todoCollection.push(data)
        this.inputLabel = ''
      })
  }

  onDeleteTodo(todoId: string) {
    this.todoService.deleteTodo(todoId)
      .subscribe(() => {
        this.todoCollection = this.todoCollection.filter(todo => todo.id !== todoId)
      })
  }

  onSave({ todoId, editLabel }) {
    this.todoService.updateTodo(todoId, editLabel)
      .subscribe(data => {
        console.log("[UPDATE]", data);
        const position = this.todoCollection.findIndex(todo => todo.id === todoId)
        this.todoCollection[position].label = editLabel;
        this.isEdit = false;
      })
  }
}
