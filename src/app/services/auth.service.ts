import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token = null;

  constructor(private router: Router) { }

  onRegister(email: string, password: string) {
    firebase.createUserWithEmailAndPassword(firebase.getAuth(), email, password)
      .then(userCredentials => {
        console.log("User Registered Successfully", userCredentials)
      }).catch(console.log)
  }

  onLogin(email: string, password: string) {
    firebase.signInWithEmailAndPassword(firebase.getAuth(), email, password)
      .then(result => {
        firebase.getAuth().currentUser.getIdToken().then(token => {
          console.log("Logged In", token);
          this.token = token;
          this.router.navigate(["/todos"])
        })
      }).catch(console.log)
  }

  getToken() {
    return this.token;
  }

  isUserAuthenticated() {
    return this.token != null;
  }

  onLogout() {
    firebase.signOut(firebase.getAuth())
      .then(resp => {
        console.log("User Logout");
        this.token = null;
        this.router.navigate(["/auth"])
      }).catch(console.log)
  }

}
