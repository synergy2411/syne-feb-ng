import { Component, Input, OnInit } from '@angular/core';
import { IComment } from 'src/app/model/comment.interface';

@Component({
  selector: 'app-user-comments',
  templateUrl: './user-comments.component.html',
  styleUrls: ['./user-comments.component.css']
})
export class UserCommentsComponent implements OnInit {

  tab = 0;
  @Input() comments: IComment[];

  constructor() { }

  ngOnInit(): void {
  }

  onAddComment(newComment: IComment) {
    this.comments.push(newComment);
    this.tab = 1;
  }

}
