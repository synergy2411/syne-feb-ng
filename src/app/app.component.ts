import { Component, OnInit } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  // encapsulation: ViewEncapsulation.None
  // providers : [DataService]
})
export class AppComponent implements OnInit {

  title = 'awesome app';

  showDialog = false;

  constructor(public authService: AuthService) { }

  ngOnInit() {
    initializeApp({
      apiKey: "AIzaSyCcZ91WPVmi41JILsFR5m0w--8bKh__cqA",
      authDomain: "syne-feb23-ng.firebaseapp.com",
    })
  }

  onClose() {
    this.showDialog = false;
  }
}
