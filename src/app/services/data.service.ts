import { Injectable } from '@angular/core';
import { USER_DATA } from '../model/mocks';
import { IUser } from '../model/user.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private userCollection: IUser[] = [];       // Stateful

  constructor() {
    this.userCollection = USER_DATA;
  }

  getUserData() {
    return this.userCollection.splice(0);
  }
}
